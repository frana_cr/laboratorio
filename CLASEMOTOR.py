#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se importa el random
import random


# Se crea la clase sobre el motor
class Motor:
    # Se genera el constructor
    def __init__(self, cilindro_motor):
        self.__cilindro_motor = (random.choice([1.2, 1.6]))

    # En este metodo suceden las comparaciones entre las cilindradas
    def comparaciones(self, cilindro_motor):
        if(cilindro_motor == 1.2):
            print("Cilindradad de", cilindro_motor, "Se consume 1L por 20 KM")
            return cilindro_motor
        else:
            print("Se consume 1L por 14 KM")
            return cilindro_motor