#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Se importa el random y las clases
import random
from CLASEAUTO import Auto
from BENCINALAB import EstanqueBencina
from clase_ruedas import Rueda
from CLASEMOTOR import Motor
from clasevelocimetro import Velocimetro

# Se genera el menu
if __name__ == '__main__':
    print("Bienvenido al auto")
    # Se establecen las variables
    bencina = 32
    ruedas = 4
    cilindro_motor = 0
    distancia = 0
    tiempo = random.randint(1, 10)
    # Se interactua con el usuario
    velocidad = int(input("Ingrese la velocidad"))
    estado = int(input("Ingresa 1: encender, Ingresa 2: apagar"))
    movimiento = int(input("INGRESE 1:DESEA MOVER, OTRO SI NO"))

    # Si el movimiento es uno se llevaran a cabo los metodos de las clases
    while movimiento == 1:
        # MOTOR
        estado_motor = Motor(cilindro_motor)
        estado_motor.set_estado_cilindro(cilindro_motor)
        estado_motor.comparaciones(cilindro_motor)
        # AUTO
        movimiento_auto = Auto(estado)
        movimiento_auto.set_estado(estado)
        movimiento_auto.estado_auto(estado)
        # ~ # VELOCIMETRO
        velocimetro_auto = Velocimetro(velocidad)
        velocimetro_auto.set_velocidad_auto1(velocidad)
        velocimetro_auto.control_velocidad(velocidad)
        # ~ #RUEDAS
        rueda_auto = Rueda(movimiento, distancia)
        rueda_auto.set_movimiento(movimiento)
        rueda_auto.set_distancia(distancia)
        rueda_auto.desgaste_rueda(movimiento, distancia)
        #BENCINA
        estanque_auto = EstanqueBencina(bencina, movimiento, cilindro_motor, velocidad, tiempo)
        estanque_auto.set_bencina(bencina)
        estanque_auto.set_movimiento(movimiento)
        estanque_auto.set_velocidad(velocidad)
        estanque_auto.set_tiempo(tiempo)
        estanque_auto.auto_parado(bencina)
        estanque_auto.consumo_real(movimiento, bencina, cilindro_motor, velocidad, tiempo)