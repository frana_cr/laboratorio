#!/usr/bin/env python
# -*- coding: utf-8 -*-
# se importa la clase motor
from CLASEMOTOR import Motor


# Se genera la clase auto
class Auto:
    # Se genera el constructor
    def __init__(self, estado):
        self.__estado = estado

    # Se crea el metodo que evalua el estado de movimiento del auto
    def estado_auto(self, estado):
        estado = int(input(" 1: DESEA ENCENDER EL AUTO, OTRO NO MOVER "))
        # Si el usuario ingresa 1 el auto parte
        if(estado == 1):
            print("EL AUTO SE ENCIENDE, AVANZA EN LINEA RECTA")
            # de modo contrario no se movera 
        else:
            print("EL AUTO NO SE MUEVE")
            self.set_estado(0) # intente solucionar este error pero no lo logre