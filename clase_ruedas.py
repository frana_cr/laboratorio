#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Se importa el random
import random


# En esta clase se lleva a cabo lo involucrado con las ruedas del auto
class Rueda:
    # Comienza el constructor
    def __init__(self, movimiento, distancia):
        self.__movimiento = movimiento
        self.__distancia = distancia

    # Se define el metodo para cambiar el movimiento
    def set_movimiento(self, movimiento):
        self.__movimiento = movimiento

    # Se define el metodo para obtener al movimiento
    def get_movimiento(self):
        return movimiento

    # Se define el metodo donde se evalua el desgaste de la rueda
    def desgaste_rueda(self, movimiento, distancia):
        if(movimiento == 1):
            # Cuando esta en movimiento esto sucede
            print("Se desgasta la rueda al andar")
            # Se desgastara aleatoriamente
            desgaste = random.randint(1, 10)
            self.set_movimiento(desgaste)
            estado_actual = desgaste + distancia
            # Cuando alcance su limite la rueda se cambiara
            if(estado_actual > 90):
                print(" SE NECESITA CAMBIO RUEDA ")
                print("SE ESTA CAMBIANDO LA RUEDA")
                self.set_estado_rueda(0)
            else:
                # De modo contrario el auto funciona bien
                print(" EL AUTO FUNCIONA BIEN ")

        # Si no se mueve el auto no hay desgaste
        else:
            print("No hay desgaste de rueda")