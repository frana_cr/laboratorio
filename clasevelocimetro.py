#!/usr/bin/env python
# -*- coding: utf-8 -*-


# Se genera la clase velocimetro
class Velocimetro:
    # Se genera el constructor
    def __init__(self, velocidad):
        self.__velocidad = velocidad

    # Con este metodo se genera el control de la velocidad
    def control_velocidad(self, velocidad):
        # Si la velocidad se excede debera ser ingresada nuevamente
        if(velocidad > 120):
            print("Ingresa nuevamente la velocidad, ALCANZO VELOCIDAD MAXIMA")
            velocidad = int(input(" Ingrese la velocidad "))

        # De lo contrario la velocidad sera correcta
        else:
            print(" VELOCIDAD CORRECTA ")